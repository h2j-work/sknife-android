package sknife.sample.views.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import sknife.sample.R
import sknife.sample.databinding.ActivityMainBinding
import sknife.sample.views.main.viewModel.MainActivityViewModel
import work.h2j.sknife.base.BaseActivityBinding


class MainActivity : BaseActivityBinding<ActivityMainBinding>() {
    override val layoutId: Int
        get() = R.layout.activity_main

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vModel = viewModel
        supportActionBar?.hide()
    }
}
