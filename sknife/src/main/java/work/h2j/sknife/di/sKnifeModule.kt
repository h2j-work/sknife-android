package work.h2j.sknife.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Suppress("ClassName")
@Module
class sKnifeModule {
    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun providePreferences(application: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(application)
}