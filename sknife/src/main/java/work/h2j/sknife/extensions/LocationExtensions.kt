package work.h2j.sknife.extensions

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import kotlin.math.*
import kotlin.random.Random

/**
 * Create a new location specified in meters and bearing from a previous location.
 * @param startLoc from where
 * @param bearing which direction, in radians from north
 * @param distance meters from startLoc
 * @return a new location
 */
fun Location.createLocation(
    bearing: Double,
    distance: Double
): Location {
    val newLocation = Location("newLocation")
    val radius = 6371000.0 // earth's mean radius in m
    val lat1 = Math.toRadians(this.latitude)
    val lng1 = Math.toRadians(this.longitude)
    val lat2 = asin(
        sin(lat1) * cos(distance / radius) + cos(lat1) * sin(
            distance / radius
        ) * cos(bearing)
    )
    var lng2 = lng1 + atan2(
        sin(bearing) * sin(distance / radius) * cos(
            lat1
        ),
        cos(distance / radius) - sin(lat1) * sin(lat2)
    )
    lng2 = (lng2 + Math.PI) % (2 * Math.PI) - Math.PI

    // normalize to -180...+180
    if (lat2 == 0.0 || lng2 == 0.0) {
        newLocation.latitude = 0.0
        newLocation.longitude = 0.0
    } else {
        newLocation.latitude = Math.toDegrees(lat2)
        newLocation.longitude = Math.toDegrees(lng2)
    }
    return newLocation
}

fun Location.getRandomLocation(radius: Double): Location {
    val x0: Double = longitude
    val y0: Double = latitude

    // Convert radius from meters to degrees.
    val radiusInDegrees: Double = radius / 111320f

    // Get a random distance and a random angle.
    val u = Random.nextDouble()
    val v = Random.nextDouble()
    val w = radiusInDegrees * sqrt(u)
    val t = 2 * Math.PI * v
    // Get the x and y delta values.
    val x = w * cos(t)
    val y = w * sin(t)

    // Compensate the x value.
    val newX = x / cos(Math.toRadians(y0))

    val foundLatitude: Double
    val foundLongitude: Double

    foundLatitude = y0 + y
    foundLongitude = x0 + newX

    val copy = Location(this)
    copy.latitude = foundLatitude
    copy.longitude = foundLongitude
    return copy
}

fun Location.toLatLng() = LatLng(latitude, longitude)