package sknife.sample.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import sknife.sample.App
import sknife.sample.di.modules.*
import work.h2j.sknife.di.sKnifeModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        sKnifeModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        AppModule::class,
        DatabaseModule::class,
        NetModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(instance: App)
}