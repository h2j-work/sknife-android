package work.h2j.sknife.api

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}