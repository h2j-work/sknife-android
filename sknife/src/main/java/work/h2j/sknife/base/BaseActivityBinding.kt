package work.h2j.sknife.base

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivityBinding<TBinding : ViewDataBinding> : BaseActivity() {
    @Suppress("RemoveExplicitTypeArguments")
    protected val binding: TBinding by lazy {
        DataBindingUtil.setContentView<TBinding>(this, layoutId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
    }
}