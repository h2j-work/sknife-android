package work.h2j.sknife.ui

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class CustomViewPager(context: Context, attributeSet: AttributeSet?) :
    ViewPager(context, attributeSet) {

    var isSwipeEnabled: Boolean = true

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (!isSwipeEnabled)
            return false

        return super.onInterceptTouchEvent(ev)
    }

    override fun performClick(): Boolean {
        if (!isSwipeEnabled)
            return false

        return super.performClick()
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (!isSwipeEnabled)
            return false

        if (ev?.action == MotionEvent.ACTION_UP)
            performClick()

        return super.onTouchEvent(ev)
    }
}