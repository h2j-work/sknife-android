package sknife.sample.di.modules

import androidx.lifecycle.ViewModel
import sknife.sample.views.main.viewModel.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import work.h2j.sknife.di.ViewModelKey


@Module
internal abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun bindHomeActivityViewModel(vm: MainActivityViewModel): ViewModel
}